// https://www.typescriptlang.org/docs/handbook/generics.html

export const map = <T, V>(fun: (a: T) => V) => (coll: T[]): V[] => {
  const result: V[] = [];
  for (const item of coll) {
    result.push(fun(item));
  }
  return result;
};

export const filter = <T>(fun: (a: T) => boolean) => (coll: T[]): T[] => {
  const result: T[] = [];
  for (const item of coll) {
    if (fun(item)) {
      result.push(item);
    }
  }
  return result;
};

export const reduce = <T, V>(fun: (a: V, b: T) => V) => (
  initialVal: V,
  coll: T[]
): V => {
  let result: V = initialVal;
  for (const item of coll) {
    result = fun(result, item);
  }
  return result;
};

export const contains = <T>(ele: T, coll: T[]): boolean => {
  for (const item of coll) {
    if (ele === item) {
      return true;
    }
  }
  return false;
};

export const unique = <T>(coll: T[]): T[] => {
  return reduce<T, T[]>(
    (accColl, ele) => (contains(ele, accColl) ? accColl : [...accColl, ele])
  )([], coll);
};

export const max = <T>(coll: T[]): T => {
  if (coll.length > 0) {
    const [first, ...rest] = coll;
    return reduce<T, T>((acc, value) => (acc > value ? acc : value))(
      first,
      rest
    );
  } else {
    throw new Error('Max : Error due to calling max fun. on empty array');
  }
};
