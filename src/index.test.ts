import { map, filter, reduce, unique, max, contains } from './index';

// map function Test cases
describe('function map<T, V>', () => {
  const double = (num: number): number => num * 2;
  it('should give empty array for empty array as input', () => {
    expect(map(double)([])).toEqual([]);
  });
  it('should double the elements in given array', () => {
    const input = [1, 2, 3, 4, 5];
    const expected = [2, 4, 6, 8, 10];
    expect(map(double)(input)).toEqual(expected);
  });
  it('should concat $ to each element in a given array', () => {
    const input = [1, 2, 3, 4, 5];
    const expected = ['$1', '$2', '$3', '$4', '$5'];
    expect(map(x => '$' + x)(input)).toEqual(expected);
  });
});

// filter function Test cases
describe('function filter<T>', () => {
  it('should give empty array for empty array as input', () => {
    expect(filter(x => x === 0)([])).toEqual([]);
  });
  it('should filter array of even numbers from a given array', () => {
    const isEven = (num: number): boolean => num % 2 === 0;
    const input = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const expected = [2, 4, 6, 8, 10];
    expect(filter(isEven)(input)).toEqual(expected);
  });
  it('should filter array of filtered strings from a given array', () => {
    const input = ['siva', 'sai', 'kumar', 'siva'];
    const expected = ['siva', 'siva'];
    expect(filter(ele => ele === 'siva')(input)).toEqual(expected);
  });
});

// reduce function Test cases
describe('function reduce<T, V>', () => {
  const addTwo = (num1: number, num2: number): number => num1 + num2;
  it('reduce on empty array gives initial value', () => {
    expect(reduce(addTwo)(0, [])).toEqual(0);
  });
  it('should add all the elements in a given an array', () => {
    expect(reduce(addTwo)(0, [1, 2, 3, 4, 5])).toEqual(15);
  });
  it('should multiply all the elements in a given an array', () => {
    const productTwo = (num1: number, num2: number): number => num1 * num2;
    expect(reduce(productTwo)(1, [1, 2, 3, 4, 5])).toEqual(120);
  });
  it('should give unique elements in given array', () => {
    const input = [1, 2, 3, 4, 5, 1, 2, 3, 4, 5, 6];
    const expected = [1, 2, 3, 4, 5, 6];
    expect(
      reduce<number, number[]>(
        (accColl, ele) => (contains(ele, accColl) ? accColl : [...accColl, ele])
      )([], input)
    ).toEqual(expected);
  });
});

// unique function Test cases
describe('function unique<T>', () => {
  it('unique<number>([]) -> []', () => {
    expect(unique<number>([])).toEqual([]);
  });
  it('unique<number>([1, 1, 2, 2, 3, 3]) -> [1, 2, 3]', () => {
    expect(unique<number>([1, 1, 2, 2, 3, 3])).toEqual([1, 2, 3]);
  });
  it('unique<string>(["siva", "sai", "siva", "siva"]) -> ["siva", "sai"]', () => {
    const input = ['siva', 'sai', 'siva', 'siva'];
    const expected = ['siva', 'sai'];
    expect(unique<string>(input)).toEqual(expected);
  });
  it('unique<boolean>([true, false, true, false]) -> [true, false]', () => {
    expect(unique<boolean>([true, false, true, false])).toEqual([true, false]);
  });
});

// max function Test cases
describe('function max', () => {
  it('max([-1]) -> -1', () => {
    expect(max([-1])).toEqual(-1);
  });
  it('max([-1, -2, -3, -4]) -> -1', () => {
    expect(max([-1, -2, -3, -4])).toEqual(-1);
  });
  it('max([1, 2, 3, 4]) -> 4', () => {
    expect(max([1, 2, 3, 4])).toEqual(4);
  });
  it('max([101, -201, 333, 1024]) -> 1024', () => {
    expect(max([101, -201, 333, 1024])).toEqual(1024);
  });
});
